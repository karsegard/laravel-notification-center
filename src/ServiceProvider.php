<?php

namespace KDA\Laravel\NotificationCenter;

use Illuminate\Support\ServiceProvider as PackageServiceProvider;


class ServiceProvider extends PackageServiceProvider
{
    public function register()
    {
        Package::make($this)->register();
    }

    //called after the trait were booted
    public function boot()
    {
        Package::get()->boot();
      

    }
}
